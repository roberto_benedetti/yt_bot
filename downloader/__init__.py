
from __future__ import unicode_literals
import youtube_dl



class MyLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


def my_hook(d):
    if d['status'] == 'finished':
        print('Done downloading, now converting ...')


ydl_opts = {
    'format': 'bestaudio/best',
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '192',
    }],
    'logger': MyLogger(),
    'progress_hooks': [my_hook],
}

class downloader():
    def __init__(self):
        self.ydl = youtube_dl.YoutubeDL(ydl_opts)

    def download(self, link:str):
        self.ydl.download(['https://www.youtube.com/watch?v=BaW_jenozKc'])
        # self.ydl.download([str])

d = downloader()
print()


